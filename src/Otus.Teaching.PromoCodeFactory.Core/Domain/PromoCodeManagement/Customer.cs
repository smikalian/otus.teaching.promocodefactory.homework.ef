﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        //TODO: Списки Preferences и Promocodes
        public virtual ICollection<CustomerPreference> Preferences { get; set; }
        public virtual PromoCode PromoCode { get; set; }
        
             
         
      
    }
}