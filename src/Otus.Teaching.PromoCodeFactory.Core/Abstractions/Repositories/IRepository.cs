﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task CreateAsync(T item);

        Task<T> UpdateAsync(T item);
        
        Task<T> DeleteAsync(T item);
        
        Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> Ids);
        
        
        
    }

    
}