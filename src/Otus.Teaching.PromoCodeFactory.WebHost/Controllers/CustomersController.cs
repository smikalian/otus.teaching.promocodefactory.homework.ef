﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        
        
        
        

        public CustomersController(IRepository<Customer> customerRepository,IRepository<Preference> preferenceRepository,  DataContext dataContext)
        {
        _customerRepository = customerRepository;
        _preferenceRepository = preferenceRepository;

            
        }
        
/// <summary>
/// Получение всех покупателей 
/// </summary>
/// <returns></returns>
        
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            //TODO: Добавить получение списка клиентов
            var customers = _customerRepository.GetAllAsync();
            var customersModelList = customers.Result.Select(x => 
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                }).ToList();

            
            return customersModelList;
            
        }
        /// <summary>
        /// Получение покупателя по Id вмнсте с выданными ему промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            
//Получение Customer
            var customer = await _customerRepository.GetByIdAsync(id);

            var customerResponce = new CustomerResponse( customer);
        
           
            return Ok(customerResponce);
        }
        
        /// <summary>
        /// Создание покупателя с его предпочтенияими
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {

            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            
            var preferences = await GetPreferencesAsync(request.PreferenceIds);

            
            Customer customer = new Customer();
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
           if (preferences != null && preferences.Any())
            {
                customer.Preferences = preferences.Select(x => new CustomerPreference()
                {
                    Customer = customer,
                    Preference = x
                }).ToList();
            }
 
            await _customerRepository.CreateAsync(customer);
            return Ok();
        }
        
        /// <summary>
        /// Обновление покупателя и его предпочтений
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            var  customer =await  _customerRepository.GetByIdAsync(id);
            var preferences = await GetPreferencesAsync(request.PreferenceIds);
            
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
           
            if (preferences != null && preferences.Any())
            {
                customer.Preferences?.Clear();
                customer.Preferences = preferences.Select(x => new CustomerPreference()
                {
                    Customer = customer,
                    Preference = x
                }).ToList();
            }


            await _customerRepository.UpdateAsync(customer);
            return Ok();

        }
        /// <summary>
        /// Удаление покупателя по его id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer != null)
            {
                await _customerRepository.DeleteAsync(customer);
            }

            return Ok();
        }
        
        private Task<IEnumerable<Preference>> GetPreferencesAsync(IEnumerable<Guid> ids)
        {
            IEnumerable<Preference> preferences = new List<Preference>();
            if (ids != null && ids.Any())
            {

                return _preferenceRepository
                    .GetRangeByIdsAsync(ids.ToList());
            }

            return Task.FromResult(preferences);
        }
    }
}