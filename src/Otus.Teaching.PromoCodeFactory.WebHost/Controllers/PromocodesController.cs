﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;
        


        public PromocodesController(IRepository<PromoCode> promocodeRepository, IRepository<Preference> preferenceRepository,
            IRepository<Customer> customerRepository,DataContext dataContext)
        {
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;

        }

        
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PromoCodeShortResponse>> GetPromocodesAsync()
        {
            //TODO: Получить все промокоды 

            var lpromocode = await _promocodeRepository.GetAllAsync();
            
            var lPromocodeShortResponce = lpromocode.Select(x =>
                new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                   ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToString(),
                    EndDate = x.EndDate.ToString(),
                    PartnerName = x.PartnerName,
                    PreferenceID = x.Preference.Id
                }).ToList();
            return  lPromocodeShortResponce ;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {

            var preference = await _preferenceRepository.GetByIdAsync(request.PreferenceId);
            
          //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            PromoCode promocode = new PromoCode();
            promocode.Code = request.PromoCode;
            promocode.BeginDate = DateTime.Today;
            promocode.EndDate = DateTime.Today;
            promocode.PartnerName = request.PartnerName;
            if (preference != null)
            {
                promocode.Preference = preference;
            }

            await _promocodeRepository.CreateAsync(promocode);

            var customers = await _customerRepository.GetAllAsync();
            foreach (var customer in customers)
            {
                var customerPreference =customer.Preferences.Where(x => x.Preference == preference).ToList();
                if (customerPreference.Count!=0)
                {
                    customer.PromoCode = promocode;
                    await _customerRepository.UpdateAsync(customer);
                }
            }
            
            

            
            return Ok();
        }
    }
}