using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;
        
        private readonly DataContext _dataContext;

        public PreferencesController(IRepository<Preference> preferenceRepository,  DataContext dataContext)
        {
            _preferenceRepository = preferenceRepository;

            _dataContext = dataContext;
        }

        /// <summary>
        /// Получение всех предпочтений  в виде id , Name
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PreferenceResponse>> GetPreferenceAsync()
        {
            //TODO: Добавить получение списка клиентов
            var preference = _preferenceRepository.GetAllAsync();
            var preferenceResponse =preference.Result.Select(x => 
                new PreferenceResponse()
                {
                    Id = x.Id,
                    Name = x.Name
                    
                }).ToList();

            
            return preferenceResponse;
            
        }
    }
}