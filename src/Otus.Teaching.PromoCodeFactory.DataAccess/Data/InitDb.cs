using System;
using System.Linq;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class InitDb: IDbInit
    {
        private readonly DataContext _dataContext;
        
        public InitDb(DataContext dataContext)
        {
            _dataContext = dataContext;
            
        }

        public  void InitializeDb()
        {

            _dataContext.AddRange(FakeDataFactory.Employees);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Preferences);
            _dataContext.SaveChanges();

           _dataContext.AddRange(FakeDataFactory.Customers);
           _dataContext.SaveChanges();   
  


        }
    }
}